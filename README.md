# Hobart TasLUG Talks
Do you have a talk that you would like to give at TasLUG? or is there a
particular topic you would like to hear more about? then [create an
issue](https://gitlab.com/taslug/hobart-talks/issues/new) and we will see what we can do.

## Welcome Slides
The welcome slides for the most recent meeting can be found at [https://taslug.gitlab.io/hobart-welcome-slides/](https://taslug.gitlab.io/hobart-welcome-slides/)

## Previous Talks
### 2020
#### [13 February 2020](https://taslug.gitlab.io/hobart-welcome-slides/20200213/)
  * Karl Goetz - [Tendenci](https://www.tendenci.com/) - The Open Source
    Association Management System (AMS)
      ([slides](https://gitlab.com/taslug/hobart-talks/uploads/2c5db5c8998710c34788ed146bd0fb27/2020-02-13_Taslug_Tendenci_presentation.odp))

### 2019
#### [10 October 2019](https://taslug.gitlab.io/hobart-welcome-slides/20191010/)
  * Simon Dwyer - [ZeroTeir](https://www.zerotier.com/)
  * John Kristensen - [Kubernetes](https://kubernetes.io/)
      ([slides](https://jerrykan-website.gitlab.io/talks/taslug2019-kubernetes/) | [speaker notes](https://gitlab.com/jerrykan-website/talks/taslug2019-kubernetes/blob/workings/outline.txt))

#### [8 August 2019](https://taslug.gitlab.io/hobart-welcome-slides/20190808/)
  * Steven Honson - [WireGuard](https://www.wireguard.com/)
  * Chris Barnard - Aurora's time-of-use tariff

#### 11 June 2019
  * Peter Billam - audio2midi: converting features of audio signals into MIDI data
      ([slides](https://pjb.com.au/midi/audio2midi_talk/index.html) | [man page](https://www.pjb.com.au/midi/audio2midi.html))
  * Karl Goetz - [Debian 10 ("Buster") release](https://www.debian.org/News/2019/20190706) party

#### [11 April 2019](https://taslug.gitlab.io/hobart-welcome-slides/20190411/)
  * Ben Short - Using Grafana and Loki with Docker
      ([slides](files/20190411/Grafana-TasLUG.pdf) | [runsheet](files/20190411/Grafana-Demo-Runsheet.txt))

#### [21 February 2019](https://taslug.gitlab.io/hobart-welcome-slides/20190221/)
  * Ian Stevenson - [Simple backups for home servers and workstations](http://istevenson.net/presentations/taslug_simplebackups.html)
  * Scott Bragg - LoRaWAN IoT

### 2018
#### [20 September 2018](https://taslug.gitlab.io/hobart-welcome-slides/20180920/)
  * Julius Roberts - Using GitLabCI to build better software
  * Peter Billam - [A collection of random topics](https://pjb.com.au/comp/2018_taslug_random_talk.html)

Video: [AnarTube (PeerTube instance)](https://tube.scriptforge.org/videos/watch/3417ce80-97ff-4282-91a9-91b1e0b70b0c)

#### [19 July 2018](https://taslug.gitlab.io/hobart-welcome-slides/20180719/)
  * Tim Serong - Monitoring electricity generation using a Raspberry Pi
  * Chris Barnard - Participcation in the [TasNetwork emPOWERing You Trial](https://www.tasnetworks.com.au/customer-engagement/tariff-reform/empoweringyou/)
  
Video: [AnarTube (PeerTube instance)](http://tube.scriptforge.org/videos/watch/2044829a-4dee-42e9-9c47-3327c0ff31e5)
