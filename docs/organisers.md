# Information for TasLUG Organisers

## Event Dates
TasLUG meet-ups are currently held on the second Thursday of each month
(excluding January and December).

Monthly meet-ups usually alternate between a meeting with one or more talks,
and a casual meet-up at pub or restaurant (TasPubLUG).

People normally arrive for meetings with speakers from 6:00pm with talks
starting at 6:30pm.

TasPubLUG meet-ups usually kick off at 6:30pm.

## Checklist - Meetings
The following is a checklist of things that need to be done in the lead-up to a
meeting.

### Four weeks before meeting
  - [Start lining up speaker(s) for the meeting](#speakers).
  - [Ensure the room at Enterprize is booked](#enterprize).

### One week before meeting
  - Speaker should be confirmed.
    - check with the speaker if there is anything they need for their
      presentation
    - if there are multiple speakers, check if they have a preferred speaking
      slot
  - [Post announcement on the TasLUG website](#website).
    - posts to the website should automatically be posted on the TasLUG Twitter
      account using [Zapier](https://zapier.com/)
  - [Send announcement to the mailing list](#mailing-list).
  - [Start working on the welcome slides](#welcome-slides).

### The day before the meeting
  - Send a reminder email to the mailing list.
  - Finalise welcome slide-deck; ensure to include:
    - any thank yous
    - information about any interesting events or conferences coming up
    - dates for any upcoming TasLUG meetings or TasPubLUGs
    - any other general items of interest

### On the day
  - Turn up before people are due to arrive and start letting people into
    Enterprize.
  - Ensure there is enough seating for attendees.
  - Assist the speaker(s) with any setup and testing they need to do.
  - Take a head-count for Joanna's records.
  - [Do a short welcome and introduction of speakers](#welcome-slides).

### Day after the event
  - Update the [`hobart-talks`
    repository](https://gitlab.com/taslug/hobart-talks) with the speaker
    information
    - include any links to slide-decks
  - Email Joanna at Enterprize the head-count from the meeting.

## Checklist - TasPubLUG
The following is a checklist of things that need to be done in the lead-up to a
TasPubLUG meet-up.

### One week before meet-up
  - Pick a venue for the meet-up
  - [Post announcement on the TasLUG website](#website).
    - posts to the website are automatically posted on the TasLUG Twitter
      account
  - [Send announcement to the mailing list](#mailing-list).

### A few days before the meet-up
  - Make a booking at the venue if needed

## Information
### Speakers
Event organisers should start trying to line up one or more speakers as far in
advance as possible. A list of potential speakers and talk ideas are keep in
the [`hobart-talks` repository
issues](https://gitlab.com/taslug/hobart-talks/issues), which might be a good
starting point for finding a talk someone is willing to give.

Try to keep the total planned talk time for speakers to ~1 hour, so that
meetings don't run too late into the evening.

### Enterprize
Meetings with speakers are usually held at
[Enterprize](https://www.enterprize.space/) Hobart. There are a number of other
user groups that make use of the space, so the earlier the presentation room
can be booked the better. The room can be booked by contacting Joanna Meyer and
bookings can be made months in advanced.

### Website
The TasLUG website is a statically generated site with the [source repository
hosted on GitLab](https://gitlab.com/taslug/taslug.gitlab.io). The
[README](https://gitlab.com/taslug/taslug.gitlab.io/blob/master/README.md)
documents how to create a new post for event announcements. If you don't have
permission to push directly to the repository, either contact one of the
[owners/maintainers](https://gitlab.com/taslug/taslug.gitlab.io/-/project_members)
to get access, or submit a pull request.

At a minimum announcements should contain the following information:
  - date and time of the event
  - location of the event
  - a brief summary of any talks/speakers at the event

The easiest thing to do is to copy a previous event announcement, change the
wording a bit, and update the relevant information.

For consistency, announcements on the website should be similar to
announcements sent to the [mailing list](#mailing-list).

### Mailing List
The [TasLUG mailing list](https://lists.linux.org.au/mailman/listinfo/taslug)
is hosted by Linux Australia. Anyone subscribed to the mailing list should have
permission to post to it.

At a minimum announcements should contain the following information:
  - date and time of the event
  - location of the event
  - a brief summary of any talks/speakers at the event

The easiest thing to do is to copy a previous event announcement, change the
wording a bit, and update the relevant information.

For consistency, announcements sent to the mailing list should be similar to
announcements posted on the [website](#website).

### Welcome Slides
The meeting organiser will generally start the meeting with a short welcome,
that includes information about any upcoming event and conferences or items of
interest, and to introduce the speakers.

The welcome will usually include some slides that contain the relevant
information if people want to follow-up about the events, conferences, or items
of interest. Organisers can choose to create their own slide-deck or use and
update the existing [reveal.js](https://revealjs.com/) based slide-deck found
in the [`hobart-welcome-slides`
repository](https://gitlab.com/taslug/hobart-welcome-slides).
